//Вішаю онКлік на батьківський єлемент, при спрацюванні викликаю дві функції
let tabs = document.getElementsByClassName('tabs')[0];
tabs.onclick = function (event) {
   let targetTab = event.target;
   activeTab(targetTab);
   activeContent(targetTab);
};

//функція яка видаляє клас active в старого таба, й вішає на "цільовий"
let selectedTab = document.getElementsByClassName('tabs-title active')[0];//задаю дефолтне значення
function activeTab(targetTab) {
   selectedTab.classList.remove('active');
   selectedTab = targetTab;//перезначаю дєфолтне значення 
   selectedTab.classList.add('active');
}

//функція яка порівнює значення Дата-атрибута у контенту й табу, при спіпадінні додає клас на active
let contents = document.getElementsByClassName('tab-content'); //масив єлементів які буду перебирати в функції
let selectedContent = document.getElementsByClassName('tab-content active')[0];//задаю дефолтне значення
function activeContent(targetTab) {
   selectedContent.classList.remove('active');
   for (let elem of contents) {
      if (elem.dataset.title === targetTab.dataset.title) {
         selectedContent = elem; //перезначаю дєфолтне значення 
         elem.classList.add('active');
      }
   }
}